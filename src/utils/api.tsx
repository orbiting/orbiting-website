import { API_BASE_URL } from "@/constants";

export async function createCommunity(data: any) {
  const res = await fetch(`${API_BASE_URL}/api/communities`, {
    method: "POST",
    body: JSON.stringify({ data }),
    headers: {
      "Content-Type": "application/json",
    },
  });
  const json = await res.json();

  return json;
}

export async function createEvent(data: any) {
  const res = await fetch(`${API_BASE_URL}/api/events`, {
    method: "POST",
    body: JSON.stringify({ data }),
    headers: {
      "Content-Type": "application/json",
    },
  });
  const json = await res.json();

  return json;
}