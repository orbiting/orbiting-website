import { GOOGLE_MAP_API_KEY } from "@/constants";

export function formatLocation(data: any) {
  const city = data?.city ?? "";
  const country = data?.country ?? "";
  const state = data?.state ?? "";

  if (!city && !country && !state) {
    return "ONLINE";
  }
  return [city, state, country].filter((x) => x).join(", ");
}

export async function getCurrentLocation(): Promise<GeolocationCoordinates | null> {
  if ("geolocation" in navigator) {
    return new Promise((resolve, reject) => {
      navigator.geolocation.getCurrentPosition((position) => {
        resolve(position.coords);
      }, reject);
    });
  } else {
    return null;
  }
}

export async function reverseGeocoding(coords: any): Promise<string | null> {
    const res = await fetch(
        `https://maps.googleapis.com/maps/api/geocode/json?latlng=${coords.latitude},${coords.longitude}&key=${GOOGLE_MAP_API_KEY}`
    );
    const response = await res.json();
    if (response.status === "OK") {
        return response.results[0].formatted_address;
    }
    return null;
}
