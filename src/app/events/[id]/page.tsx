import { GOOGLE_MAP_API_KEY } from "@/constants";
import { formatLocation } from "@/utils/helpers";
import { Metadata } from "next";

interface IProps {
  params: {
    id: string;
  };
}

export async function generateMetadata({ params }: IProps): Promise<Metadata> {
  const res = await fetch(`https://apiv2.orbiting.in/api/events/${params.id}`);
  const response = await res.json();

  const title = response?.name ?? "";
  const description = response?.description ?? "";
  const image =
    response?.banner.length > 0
      ? response?.banner[0].url
      : "https://orbiting.in/logo.png";

  return {
    title,
    description,
    openGraph: {
      images: image,
    },
  };
}

export default async function Page({ params }: IProps) {
  const res = await fetch(`https://apiv2.orbiting.in/api/events/${params.id}`);
  const response = await res.json();

  return (
    <main className="max-w-5xl mx-auto px-4 sm:px-6 lg:px-8">
      <div className="relative">
        <div
          className="w-full h-[30rem] flex flex-col-reverse gradient-box rounded-xl shadow-xl overflow-hidden"
          style={{
            background: `url(${
              response?.banner.length > 0 ? response?.banner[0].url : ""
            })`,
          }}
        >
          <div className="flex justify-between items-center z-10 p-8">
            <div>
              <h1 className="text-4xl font-bold text-white">
                {response?.name}
              </h1>
              <h3 className="text-white text-lg mt-2">
                {formatLocation(response.location)}
              </h3>
            </div>
            <button className="bg-blue-500 text-white px-12 py-3 text-lg rounded-lg whitespace-nowrap">
              Visit Event
            </button>
          </div>
        </div>
      </div>
      <div className="mt-8">
        <h1 className="font-bold text-2xl mb-2">Description</h1>
        <p>{response.description}</p>
        <h1 className="font-bold text-2xl mb-2 mt-8">Event Location</h1>
        <iframe
          height="450"
          style={{ border: 0, width: "100%" }}
          loading="lazy"
          allowFullScreen
          referrerPolicy="no-referrer-when-downgrade"
          src={`https://www.google.com/maps/embed/v1/place?key=${GOOGLE_MAP_API_KEY}
    &q=${response?.location?.latitude},${response?.location?.longitude}`}
        ></iframe>
      </div>
    </main>
  );
}
