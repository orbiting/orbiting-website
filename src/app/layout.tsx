import "./globals.css";
import { Inter } from "next/font/google";

const inter = Inter({ subsets: ["latin"] });

export const metadata = {
  title: "Orbiting",
  description: "A platform for sharing and discovering people and ideas.",
};

export default function RootLayout({
  children,
}: {
  children: React.ReactNode;
}) {
  return (
    <html lang="en">
      <body className={inter.className}>
        <div className="flex w-screen p-2 items-center fixed left-0 top-0 bg-white">
          <img src="/logo.png" className="w-12 h-12" />
          <h1 className="ml-2 text-2xl font-bold">Orbiting</h1>
        </div>
        <div className="mt-16" />
        {children}
      </body>
    </html>
  );
}
