import InstallApp from "@/components/install_app";
import { Metadata } from "next";

interface IProps {
  params: {
    id: string;
  };
}

export async function generateMetadata({ params }: IProps): Promise<Metadata> {
  const res = await fetch(`https://apiv2.orbiting.in/api/posts/${params.id}`);
  const response = await res.json();

  const title = `Post by ${response.posted_by?.name} - Orbiting`;
  const description = response.description;
  const image = response.image.length > 0 ? response.image[0].url : "";

  return {
    title,
    description,
    openGraph: {
      images: image,
    },
  };
}

export default function Page() {
  return (
    <main>
      <InstallApp />
    </main>
  );
}
