import OrbitDetailOG from "@/components/orbit_detail_og";
import { ImageResponse } from "next/server";

export const runtime = "edge";

export async function GET(request: Request) {
  const { searchParams } = new URL(request.url);
  const title = searchParams.get("title") ?? "Orbiting";
  const description =
    searchParams.get("description") ??
    "Orbiting is a social network for the future";
  const image = searchParams.get("image") ?? "https://orbiting.in/logo.png";
  const memberCount = searchParams.get("memberCount") ?? "10 million";

  return new ImageResponse(
    (
      <OrbitDetailOG
        title={title}
        description={description}
        image={image}
        memberCount={memberCount}
      />
    ),
    {
      width: 800,
      height: 400,
    }
  );
}
