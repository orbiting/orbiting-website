"use client";

import QkDropdown from "@/qk_form/qk_dropdown";
import QkImagePicker from "@/qk_form/qk_image_picker";
import LocationPicker from "@/qk_form/qk_location_picker";
import QkTextArea from "@/qk_form/qk_text_area";
import QkTextField from "@/qk_form/qk_text_field";
import { createCommunity, createEvent } from "@/utils/api";
import Lottie from "lottie-react";
import Head from "next/head";
import { useState } from "react";
import loadingAnimation from "@/utils/loading.json";

interface EventSchema {
  name: string;
  description: string;
  banner: number[];
  location: {
    latitude: string;
    longitude: string;
    address: string;
  };
  event_url: string;
  start_time: string;
  end_time: string;
  event_type: "ONLINE" | "OFFLINE";
}

export default function Page() {
  const [value, setValue] = useState<EventSchema>({
    name: "",
    description: "",
    banner: [],
    location: {
      latitude: "0",
      longitude: "0",
      address: "",
    },
    event_url: "",
    start_time: "",
    end_time: "",
    event_type: "ONLINE",
  });
  const [loading, setLoading] = useState(false);

  const handleSubmit = async () => {
    if (
      !value.name ||
      !value.description ||
      !value.banner ||
      !value.location.latitude ||
      !value.location.longitude ||
      !value.event_url ||
      !value.start_time ||
      !value.end_time ||
      !value.event_type
    ) {
      alert("Please fill all the fields");
      return;
    }
    setLoading(true);
    try {
      await createEvent(value);
    } catch (e) {
      console.log(e);
    }
    setLoading(false);
    window.location.reload();
  };

  return (
    <>
      <Head>
        <title>List your Event - Soloco</title>
        <meta
          name="description"
          content="Get your event promoted using our platform."
        />
      </Head>
      <main className="max-w-3xl mx-auto flex flex-col items-center">
        <h1 className="text-3xl font-bold">List your event</h1>
        <p className="text-gray-500 mt-2">
          Get your event promoted using our platform.
        </p>
        <div className="grid grid-cols-2 mt-8 gap-8 w-full">
          <div className="col-span-2">
            <QkTextField
              label="Name"
              value={value.name}
              onChange={(data) =>
                setValue({
                  ...value,
                  name: data,
                })
              }
            />
          </div>
          <div className="col-span-2">
            <QkTextArea
              label="Description"
              value={value.description}
              onChange={(data) =>
                setValue({
                  ...value,
                  description: data,
                })
              }
            />
          </div>
          <div className="col-span-2">
            <QkTextField
              label="Event Link"
              value={value.event_url}
              type="url"
              onChange={(data) =>
                setValue({
                  ...value,
                  event_url: data,
                })
              }
            />
          </div>
          <div className="col-span-2">
            <LocationPicker
              label="Event Location"
              value={value.location}
              onChange={(data) =>
                setValue({
                  ...value,
                  location: data,
                })
              }
            />
          </div>
          <div>
            <QkImagePicker
              label="Event Image"
              value={value.banner[0] ?? null}
              onChange={(data) =>
                setValue({
                  ...value,
                  banner: [data],
                })
              }
            />
          </div>
          <div>
            <QkDropdown
              label="Event Type"
              value={value.event_type}
              onChange={(data: any) =>
                setValue({
                  ...value,
                  event_type: data,
                })
              }
              items={[
                {
                  value: "Online",
                  key: "ONLINE",
                },
                {
                  value: "Offline",
                  key: "OFFLINE",
                },
              ]}
            />
          </div>
          <div className="col-span-2">
            <button
              className="bg-blue-500 text-white text-lg py-3 rounded-md w-full"
              onClick={handleSubmit}
            >
              Create Event
            </button>
          </div>
        </div>
        {loading && (
          <div className="fixed z-20 w-screen h-screen flex justify-center items-center bg-black bg-opacity-90">
            <Lottie animationData={loadingAnimation} loop={true} />;
          </div>
        )}
      </main>
    </>
  );
}
