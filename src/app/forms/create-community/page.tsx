"use client";

import QkDropdown from "@/qk_form/qk_dropdown";
import QkImagePicker from "@/qk_form/qk_image_picker";
import LocationPicker from "@/qk_form/qk_location_picker";
import QkTextArea from "@/qk_form/qk_text_area";
import QkTextField from "@/qk_form/qk_text_field";
import { createCommunity } from "@/utils/api";
import Lottie from "lottie-react";
import Head from "next/head";
import { useState } from "react";
import loadingAnimation from "@/utils/loading.json";

interface CommunitySchema {
  name: string;
  description: string;
  image: number;
  location: {
    latitude: string;
    longitude: string;
    address: string;
  };
  link: string;
  type: "PUBLIC" | "PRIVATE";
}

export default function Page() {
  const [value, setValue] = useState<CommunitySchema>({
    name: "",
    description: "",
    image: 0,
    location: {
      latitude: "0",
      longitude: "0",
      address: "",
    },
    link: "",
    type: "PUBLIC",
  });
  const [loading, setLoading] = useState(false);

  const handleSubmit = async () => {
    if (
      !value.name ||
      !value.description ||
      !value.image ||
      !value.location.latitude ||
      !value.location.longitude ||
      !value.link ||
      !value.type
    ) {
      alert("Please fill all the fields");
      return;
    }
    setLoading(true);
    try {
      await createCommunity(value);
    } catch (e) {
      console.log(e);
    }
    setLoading(false);
    window.location.reload();
  };

  return (
    <>
      <Head>
        <title>List your Community - Soloco</title>
        <meta
          name="description"
          content="Create a community for anything you like."
        />
      </Head>
      <main className="max-w-3xl mx-auto flex flex-col items-center">
        <h1 className="text-3xl font-bold">List your community</h1>
        <p className="text-gray-500 mt-2">
          Create a community for anything you like.
        </p>
        <div className="grid grid-cols-2 mt-8 gap-8 w-full">
          <div className="col-span-2">
            <QkTextField
              label="Name"
              value={value.name}
              onChange={(data) =>
                setValue({
                  ...value,
                  name: data,
                })
              }
            />
          </div>
          <div className="col-span-2">
            <QkTextArea
              label="Description"
              value={value.description}
              onChange={(data) =>
                setValue({
                  ...value,
                  description: data,
                })
              }
            />
          </div>
          <div className="col-span-2">
            <QkTextField
              label="Community Link"
              value={value.link}
              type="url"
              onChange={(data) =>
                setValue({
                  ...value,
                  link: data,
                })
              }
            />
          </div>
          <div className="col-span-2">
            <LocationPicker
              label="Community Location"
              value={value.location}
              onChange={(data) =>
                setValue({
                  ...value,
                  location: data,
                })
              }
            />
          </div>
          <div>
            <QkImagePicker
              label="Community Image"
              value={value.image}
              onChange={(data) =>
                setValue({
                  ...value,
                  image: data,
                })
              }
            />
          </div>
          <div>
            <QkDropdown
              label="Community Type"
              value={value.type}
              onChange={(data: any) =>
                setValue({
                  ...value,
                  type: data,
                })
              }
              items={[
                {
                  key: "PUBLIC",
                  value: "Public",
                },
                {
                  key: "PRIVATE",
                  value: "Private",
                },
              ]}
            />
          </div>
          <div className="col-span-2">
            <button
              className="bg-blue-500 text-white text-lg py-3 rounded-md w-full"
              onClick={handleSubmit}
            >
              Create Community
            </button>
          </div>
        </div>
        {loading && (
          <div className="fixed z-20 w-screen h-screen flex justify-center items-center bg-black bg-opacity-90">
            <Lottie animationData={loadingAnimation} loop={true} />;
          </div>
        )}
      </main>
    </>
  );
}
