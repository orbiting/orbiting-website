import InstallApp from "@/components/install_app";

export default function Home() {
  return (
    <main className="">
      <InstallApp />
    </main>
  );
}
