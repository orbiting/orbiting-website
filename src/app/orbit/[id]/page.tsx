import InstallApp from "@/components/install_app";
import { Metadata } from "next";

interface IProps {
  params: {
    id: string;
  };
}

export async function generateMetadata({ params }: IProps): Promise<Metadata> {
  const res = await fetch(`https://apiv2.orbiting.in/api/orbits/${params.id}`);
  const response = await res.json();

  const title = response.orbit?.name ?? "";
  const description = response.orbit?.about ?? "";
  const image = response.orbit?.logo?.url ?? "https://orbiting.in/logo.png";
  const memberCount = response.followers_count ?? 0;

  return {
    title: response.orbit?.name ?? "",
    description: response.orbit?.about ?? "",
    openGraph: {
      images: `/og?title=${title}&description=${description}&image=${image}&memberCount=${memberCount}`,
    },
  };
}

export default function Page({ params }: IProps) {
  return (
    <main>
      <InstallApp />
    </main>
  );
}
