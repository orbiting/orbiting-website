'use client'
interface QkTextFieldProps {
  label: string;
  value: string;
  onChange: (value: string) => void;
  type?: string;
}

export default function QkTextField(props: QkTextFieldProps) {
  return (
    <div className="flex flex-col">
      <label className="text-lg text-gray-500 mb-1">{props.label}</label>
      <input
        type={props.type ?? "text"}
        className="h-12 rounded-lg shadow-xl px-4 border-0"
        value={props.value}
        onChange={(e) => props.onChange(e.target.value)}
        placeholder={`Enter ${props.label}`}
      />
    </div>
  );
}
