import { useEffect, useState } from "react";

interface QkImagePickerProps {
  label: string;
  value: number;
  onChange: (value: number) => void;
}

export default function QkImagePicker(props: QkImagePickerProps) {
  const [image, setImage] = useState<File | null>(null);

  useEffect(() => {
    if (!image) return;

    // Upload image to server
    const formData = new FormData();
    formData.append("files", image);
    fetch("https://apiv2.orbiting.in/api/upload", {
      method: "POST",
      body: formData,
    })
      .then((res) => res.json())
      .then((data) => {
        props.onChange(data[0].id);
      });
  }, [image]);

  return (
    <div className="flex flex-col">
      <label className="text-lg text-gray-500 mb-1">{props.label}</label>
      <div className="border-dashed p-4 border-2 border-gray-500 rounded-lg">
        {image ? (
          <div className="relative p-8">
            <img src={URL.createObjectURL(image)} className="w-full" />
            <button
              style={{
                top: 0,
                right: 0,
                position: "absolute",
                zIndex: 10,
                backgroundColor: "#ff0000",
                color: "#ffffff",
                padding: "8px 16px",
              }}
              onClick={() => {
                setImage(null);
              }}
            >
              Delete
            </button>
          </div>
        ) : (
          <input
            type="file"
            className="h-12 rounded-lg shadow-xl px-4 py-2 w-full"
            onChange={(e) => {
              if (e.target.files) {
                setImage(e.target.files.item(0));
              }
            }}
            placeholder={`Enter ${props.label}`}
          />
        )}
      </div>
    </div>
  );
}
