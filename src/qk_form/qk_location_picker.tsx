"use client";

import React, { useEffect, useState } from "react";
import {
  APIProvider,
  Map,
  AdvancedMarker,
  useAdvancedMarkerRef,
} from "@vis.gl/react-google-maps";
import { GOOGLE_MAP_API_KEY } from "@/constants";
import { getCurrentLocation, reverseGeocoding } from "@/utils/helpers";

interface QkLocationPickerProps {
  label: string;
  value: {
    latitude: string;
    longitude: string;
    address: string;
  };
  onChange: (value: any) => void;
}

export default function QkLocationPicker(props: QkLocationPickerProps) {
  const [position, setPosition] = useState({ lat: 53.54992, lng: 10.00678 });
  const [markerRef, marker] = useAdvancedMarkerRef();
  const [address, setAddress] = useState("Asansol, West Bengal, India");
  const [suggestions, setSuggestions] = useState<any[]>([]);

  useEffect(() => {
    (async () => {
      try {
        const currentLocation = await getCurrentLocation();
        if (currentLocation) {
          setPosition({
            lat: currentLocation.latitude,
            lng: currentLocation.longitude,
          });
          marker?.setPosition({
            lat: currentLocation.latitude,
            lng: currentLocation.longitude,
          });
        }
      } catch (e) {
        console.log(e);
      }
    })();
  }, []);

  useEffect(() => {
    reverseGeocoding({ latitude: position.lat, longitude: position.lng }).then(
      (add) => {
        setAddress(add ?? "");
        props.onChange({
          latitude: position.lat.toString(),
          longitude: position.lng.toString(),
          address: add ?? "",
        });
      }
    );
  }, [position]);

  return (
    <>
      <label className="text-lg text-gray-500">{props.label}</label>
      <input
        type="text"
        className="w-full h-10 overflow-hidden border-1 rounded-lg border-blue-500 outline-none text-sm mt-2"
        value={address}
      />
      <div className="w-full h-[30rem] flex flex-col-reverse rounded-xl shadow-xl overflow-hidden mt-2">
        <APIProvider apiKey={GOOGLE_MAP_API_KEY}>
          <Map center={position} zoom={10} mapId={btoa(position.toString())}>
            <AdvancedMarker
              position={position}
              ref={markerRef}
              gmpDraggable={true}
              anchor="bottom"
              onDragEnd={(value: any) => setPosition(value.latLng.toJSON())}
            />
          </Map>
        </APIProvider>
      </div>
    </>
  );
}
