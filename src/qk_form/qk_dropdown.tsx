interface QkDropdownProps {
  label: string;
  value: string;
  onChange: (value: string) => void;
  items: {
    key: any;
    value: string;
  }[];
}

export default function QkDropdown(props: QkDropdownProps) {
  return (
    <div>
      <label className="text-lg text-gray-500">
        {props.label}
      </label>
      <select
        className="mt-1 block w-full pl-3 pr-10 py-3 text-base border-gray-300 focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm rounded-md"
        defaultValue={props.items[0].key}
        onChange={(e) => {
          props.onChange(e.target.value);
        }}
      >
        {props.items.map((item) => (
          <option value={item.key}>{item.value}</option>
        ))}
      </select>
    </div>
  );
}
