interface QkTextAreaProps {
  label: string;
  value: string;
  onChange: (value: string) => void;
}

export default function QkTextArea(props: QkTextAreaProps) {
  return (
    <div className="flex flex-col">
      <label className="text-lg text-gray-500 mb-1">{props.label}</label>
      <textarea
        className="h-12 rounded-lg shadow-xl border-0"
        style={{ padding: "0.75rem" }}
        rows={5}
        placeholder={`Enter ${props.label}`}
        value={props.value}
        onChange={(e) => props.onChange(e.target.value)}
      ></textarea>
    </div>
  );
}
