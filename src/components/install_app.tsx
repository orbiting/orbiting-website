import { ArrowRight } from "./arrow_right";

export default function InstallApp() {
  return (
    <div className="flex flex-col md:flex-row items-center h-screen w-screen justify-center">
      <img src="/qr-code.png" alt="QR Code" className="w-72" />
      <div className="ml-4 flex flex-col items-center md:items-start">
        <h1 className="text-3xl font-bold">Orbiting</h1>
        <p className="mt-1 text-center">
          A platform for sharing and discovering people and ideas.
        </p>
        <p className="mt-4 text-sm">
          Install the app by scanning the qr code provided.
        </p>
        <div className="mt-4 border-2 border-green-500 rounded-md overflow-hidden inline-flex items-stretch w-full flex-nowrap">
          <input
            type="text"
            className="w-full focus:outline-none border-0 ml-4"
            placeholder="Enter Mobile Number"
          />
          <button className="bg-green-500 text-white px-4 py-2 inline-flex items-center">
            <span className="whitespace-nowrap">Send Link</span>
            <ArrowRight className="ml-2 h-5 w-5" />
          </button>
        </div>
      </div>
    </div>
  );
}
