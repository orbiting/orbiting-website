interface IProps {
  title: string;
  description: string;
  image: string;
  memberCount: string;
}

export default function OrbitDetailOG(props: IProps) {
  return (
    <div tw="flex flex-col bg-white w-full h-full items-center justify-between">
      <div tw="flex flex-row-reverse w-full items-center">
        <h1 tw="text-xl font-bold ml-2 mr-4">Orbiting</h1>
        <img src="https://orbiting.in/logo.png" tw="w-10 h-10" />
      </div>
      <div tw="flex items-center">
        <img
          src={props.image}
          tw="w-36"
        />
        <div tw="flex flex-col ml-6">
          <h1 tw="font-bold text-3xl mb-0">{props.title}</h1>
          <p tw="mt-1 mb-0">{props.description}</p>
          <div tw="flex flex-row items-center mt-1">
            <svg
              xmlns="http://www.w3.org/2000/svg"
              viewBox="0 0 24 24"
              fill="currentColor"
            //   @ts-ignore
              tw="w-6 h-6"
            >
              <path
                fillRule="evenodd"
                d="M8.25 6.75a3.75 3.75 0 117.5 0 3.75 3.75 0 01-7.5 0zM15.75 9.75a3 3 0 116 0 3 3 0 01-6 0zM2.25 9.75a3 3 0 116 0 3 3 0 01-6 0zM6.31 15.117A6.745 6.745 0 0112 12a6.745 6.745 0 016.709 7.498.75.75 0 01-.372.568A12.696 12.696 0 0112 21.75c-2.305 0-4.47-.612-6.337-1.684a.75.75 0 01-.372-.568 6.787 6.787 0 011.019-4.38z"
                clipRule="evenodd"
              />
              <path d="M5.082 14.254a8.287 8.287 0 00-1.308 5.135 9.687 9.687 0 01-1.764-.44l-.115-.04a.563.563 0 01-.373-.487l-.01-.121a3.75 3.75 0 013.57-4.047zM20.226 19.389a8.287 8.287 0 00-1.308-5.135 3.75 3.75 0 013.57 4.047l-.01.121a.563.563 0 01-.373.486l-.115.04c-.567.2-1.156.349-1.764.441z" />
            </svg>

            <p tw="mt-2 mb-1 ml-1">{props.memberCount} members</p>
          </div>
          <p tw="rounded-full bg-blue-500 text-white font-bold text-sm px-4 py-2 w-36">
            View in Orbiting
          </p>
        </div>
      </div>
      <div tw="flex flex-row justify-center items-center mb-2">
        Orbiting 2023. All Rights reserved
      </div>
    </div>
  );
}
